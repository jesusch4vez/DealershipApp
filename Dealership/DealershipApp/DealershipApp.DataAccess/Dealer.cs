﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DealershipApp.DataAccess
{
    public class Dealer
    {
        public Vehicle vehicle { get; set; }
        public Dealership dealership { get; set; }
        public Account account { get; set; }
        public Dealer()
        {
            vehicle = new Vehicle();
            dealership = new Dealership();
            account = new Account();

        }
    }
}
