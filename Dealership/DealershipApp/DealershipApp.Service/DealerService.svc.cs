﻿using DealershipApp.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace DealershipApp.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class DealerService : IDealerService
    {
        DealerDB db = new DealerDB();
        public IEnumerable<VehicleDAO> GetVehicles()
        {
            var v = db.getVehicles();
            List<VehicleDAO> ret = new List<VehicleDAO>();
            foreach(var item in v)
            {
                ret.Add(new VehicleDAO()
                {
                    VehicleId = item.VehicleId,
                    DealershipId = item.DealershipId,
                    Make = item.Make,
                    Model = item.Model,
                    Year = item.Year,
                    Color = item.Color,
                    Condition = item.Condition,
                    Type = item.Type,
                    Price = item.Price,
                    Mileage = item.Mileage,
                    Latitude = System.Convert.ToDouble(item.Latitude),
                    Longitude = System.Convert.ToDouble(item.Longitude),
                    Address = item.Address,
                    City = item.City,
                    State = item.State,
                    Zip = item.Zip,
                    Phone = item.Phone,
                    Name = item.Name
                });
            }           
            return ret;
        }
        
    }
}
