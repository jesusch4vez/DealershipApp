﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace DealershipApp.Service
{
    [DataContract]
    public class VehicleDAO
    {
        [DataMember]
        public int VehicleId { get; set; }
        [DataMember]
        public int DealershipId { get; set; }
        [DataMember]
        public string Make { get; set; }
        [DataMember]
        public string Model { get; set; }
        [DataMember]
        public string Year { get; set; }
        [DataMember]
        public string Color { get; set; }
        [DataMember]
        public string Condition { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public float Price { get; set; }
        [DataMember]
        public int Mileage { get; set; }
        [DataMember]
        public double Latitude { get; set; }
        [DataMember]
        public double Longitude { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public string Zip { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Name { get; set; }

    }
}