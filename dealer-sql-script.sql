--use master;
--go

--drop database DealershipDB;

--create database DealershipDB;
--go

--use DealershipDb;
--go

use master;
go

use DealershipDB;
go

create schema Dealer;
go

create table Dealer.Vehicle(
  VehicleId int not null identity(1,1),
  DealershipId int not null,
  Make nvarchar(50) not null,
  Model nvarchar(50) not null,
  [Year] nvarchar(50) not null,
  Color nvarchar(50) not null,
  Condition nvarchar(20) not null,
  [Type] nvarchar(50) not null,
  Price float(2) not null,
  Mileage int not null
)

create table Dealer.Dealership(
  DealershipId int not null identity(1,1),
  [Address] nvarchar(50) not null,
  City nvarchar(50) not null,
  [State] nvarchar(2) not null,
  Zip nvarchar(5) not null,
  Latitude decimal(10,6) not null,
  Longitude decimal(10,6) not null,
  Name nvarchar(50) not null,
  Phone nvarchar(12) not null
)

create table Dealer.Account(
  AccountId int not null identity(1,1),
  Username nvarchar(50) not null,
  [Password] nvarchar(50) not null,
  Email nvarchar(50) not null,
  DealershipId int not null
)

--set the primary
alter table Dealer.Vehicle
add constraint pk_vehicle_vehicleid primary key clustered(VehicleId);

alter table Dealer.Dealership
add constraint pk_dealership_dealershipid primary key clustered(DealershipId);

alter table Dealer.Account
add constraint pk_account_accountid primary key clustered(AccountId);

--set foriegn 
alter table Dealer.Vehicle
add constraint fk_vehicle_dealershipid
foreign key(DealershipId) references Dealer.Dealership(DealershipId);
go

alter table Dealer.Account
add constraint fk_account_dealershipid
foreign key(DealershipId) references Dealer.Dealership(DealershipId);


create view Dealer.VehicleFull as 
select VehicleId, Dealer.Vehicle.DealershipId, Make, Model, [Year], Color, Condition, [Type], Price, Mileage, Latitude, Longitude,
  [Address], City, [State], Zip, Phone, Name
  from Dealer.Vehicle inner join Dealer.Dealership on Dealer.Vehicle.DealershipId = Dealer.Dealership.DealershipId;
go

insert into Dealer.Dealership([Address], City, [State], Zip, Latitude, Longitude, Name, Phone)
values ('123 Elm St', 'Reston', 'VA', '20191',  38.948449, -77.342215, 'Freddys Dealership', '202-432-9595');

insert into Dealer.Dealership([Address], City, [State], Zip, Latitude, Longitude, Name, Phone)
values ('4587 Lucio Ave', 'Springfield', 'VA', '22150',  38.781571, -77.183921, 'Lucios Used Car Lot', '301-857-3819');

insert into Dealer.Dealership([Address], City, [State], Zip, Latitude, Longitude, Name, Phone)
values ('9438 Winston Rd', 'Richmond', 'VA', '23218',  37.531399, -77.476009, 'Winstons New and Preowned Cars', '202-941-5532');

insert into Dealer.Dealership([Address], City, [State], Zip, Latitude, Longitude, Name, Phone)
values ('5561 Bastion Blvd', 'Annapolis', 'MD', '21401',  38.972281, -76.506396, 'Bastions Buckets', '410-505-9472');

insert into Dealer.Dealership([Address], City, [State], Zip, Latitude, Longitude, Name, Phone)
values ('7381 Junkrat St', 'Baltimore', 'MD', '21117',  39.300213, -76.610516, 'Junkrats Automotive', '443-451-7611');


insert into Dealer.Vehicle(DealershipId, Make, Model,[Year], Color, Condition, [Type], Price, Mileage)
values (1, 'Honda', 'Accord', 1998, 'Black', 'Used', 'Sedan', 4500.00, 137319);

insert into Dealer.Vehicle(DealershipId, Make, Model,[Year], Color, Condition, [Type], Price, Mileage)
values (3, 'Honda', 'Accord', 2004, 'White', 'New', 'Sedan', 8800.00, 34192);

insert into Dealer.Vehicle(DealershipId, Make, Model,[Year], Color, Condition, [Type], Price, Mileage)
values (4, 'Ford', 'Focus', 2006, 'Grey', 'Used', 'Coupe', 750.00, 50152);

insert into Dealer.Vehicle(DealershipId, Make, Model,[Year], Color, Condition, [Type], Price, Mileage)
values (5, 'Honda', 'Accord', 2010, 'Tan', 'Used', 'Sedan', 9500.00, 12910);

insert into Dealer.Vehicle(DealershipId, Make, Model,[Year], Color, Condition, [Type], Price, Mileage)
values (1, 'Chevrolet', 'Corvette', 2009, 'Yellow', 'Used', 'Coupe', 55050.00, 30191);

insert into Dealer.Vehicle(DealershipId, Make, Model,[Year], Color, Condition, [Type], Price, Mileage)
values (2, 'Toyota', 'Matrix', 2007, 'Blue', 'Used', 'Hatchback', 20910.00, 40191);

insert into Dealer.Vehicle(DealershipId, Make, Model,[Year], Color, Condition, [Type], Price, Mileage)
values (3, 'Nissan', 'Sentra', 2005, 'Black', 'Used', 'Sedan', 10500.00, 69207);

insert into Dealer.Vehicle(DealershipId, Make, Model,[Year], Color, Condition, [Type], Price, Mileage)
values (5, 'Tesla', 'Model S', 2014, 'White', 'New', 'Sedan', 30300.00, 0);

--delete from Dealer.Vehicle where Year = 2010;
select * from Dealer.VehicleFull;

select * from Dealer.Vehicle;

select * from Dealer.Dealership;
select * from Dealer.Account;
delete from Dealer.Account where DealershipId = 21;
delete from Dealer.Dealership where DealershipId = 21;
delete from Dealer.Vehicle where VehicleId = 13;

insert into Dealer.Account(DealershipId, Username, [Password], Email)
values (1, 'Freddy', 'Freddy', 'fred@gmail.com');

insert into Dealer.Account(DealershipId, Username, [Password], Email)
values (2, 'Lucio', 'Lucio', 'lucio@yahoo.com');

insert into Dealer.Account(DealershipId, Username, [Password], Email)
values (3, 'Winston', 'Winston', 'winston@hotmail.com');

insert into Dealer.Account(DealershipId, Username, [Password], Email)
values (4, 'Bastion', 'Bastion', 'bastion@gmail.com');

insert into Dealer.Account(DealershipId, Username, [Password], Email)
values (5, 'Junkrat', 'Junkrat', 'junkrat@gmail.com');
--alter table Dealer.Dealership add Name nvarchar(50);

--update Dealer.Dealership set Name = 'Bobs Used Car Lot' where Zip='22150';